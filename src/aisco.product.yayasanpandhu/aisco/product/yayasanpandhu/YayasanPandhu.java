package aisco.product.yayasanpandhu;

import aisco.feature.selection.FeatureSelection;
import aisco.program.activity.Program;
import aisco.program.ProgramFactory;
import aisco.financialreport.core.FinancialReport;
import aisco.financialreport.FinancialReportFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class YayasanPandhu {

    public static void main(String[] args) {

        //Select features by calling default constructor
        //Product Yayasan Pandhu consists of Feature (Activity, Income)
        FeatureSelection features = new FeatureSelection();
        Program program = ProgramFactory.createProgram("aisco.program.activity.ProgramImpl");    
        FinancialReport income = FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl");
        
        features.addFeatures(program);
        features.addFeatures(income);

        //Product Validation (Check Feature's constraints)
        try {
            features.productValidation("Yayasan Pandhu");
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(10);
        }

        //Create instance of Program
        Program sharegifts = ProgramFactory.createProgram("aisco.program.activity.ProgramImpl",1, "Share Gifts", "Sharing gifts to slum area", "50 persons", "Pandhu Volunteer", "https://www.yaysanpandhu.splelive.id/pandhu.jpg");
        Program orphanagecompetition = ProgramFactory.createProgram("aisco.program.activity.ProgramImpl",2, "Orphanage Competition", "Competition of Orphanage in Karawang", "30 orphanages", "Government", "https://www.yaysanpandhu.splelive.id/panti.jpg");

        //Create instance of Incomes
        List<FinancialReport> incomes = new ArrayList<>();
        incomes.add(FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl", "1", "12-11-2019", 2500000, "Donation from Society", sharegifts, "11000", "Cash"));
        incomes.add(FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl", "2", "13-11-2019", 300000, "Donation Raden", orphanagecompetition, "11000", "Transfer"));
        incomes.add(FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl", "3", "14-11-2019", 500000, "Donation Anonim", sharegifts, "110", "Transfer"));
        System.out.println("\n Income Report");
        System.out.println(Arrays.asList(incomes));
        income.printTotal();


    }
}
