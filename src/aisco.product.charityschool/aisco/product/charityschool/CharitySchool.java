package aisco.product.charityschool;

import aisco.feature.selection.FeatureSelection;
import aisco.program.activity.Program;
import aisco.program.ProgramFactory;
import aisco.financialreport.core.FinancialReport;
import aisco.financialreport.FinancialReportFactory;
import aisco.donation.core.Donation;
import aisco.donation.DonationFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class CharitySchool {
    FeatureSelection features;
    List<Object> featuresList;

    public static void main(String[] args) {

        //Select features by calling default constructor
        //Product Charity School consists of Feature (Activity, Operational, Income, Expense)
        FeatureSelection features = new FeatureSelection();
        Program program = ProgramFactory.createProgram("aisco.program.activity.ProgramImpl");
        Program operational = ProgramFactory.createProgram("aisco.program.operational.ProgramImpl");
        FinancialReport income = FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl");
        FinancialReport expense = FinancialReportFactory.createFinancialReport("aisco.financialreport.expense.FinancialReportImpl");
        Donation donate = DonationFactory.createDonation("aisco.donation.pgateway.DonationImpl");
        
        features.addFeatures(program);
        features.addFeatures(operational);
        features.addFeatures(income);
        features.addFeatures(expense);
        features.addFeatures(donate);


        //Product Validation (Check Feature's constraints)
        try {
            features.productValidation("Charity School");
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(10);
        }

        //Create instance of program
        List<Program> programs = new ArrayList<>();
        Program schoolconstruction = ProgramFactory.createProgram("aisco.program.activity.ProgramImpl", 1, "School Construction", "Construct the building of elementary school", "100 students", "Government", "https://www.myschool.splelive.id/logo");
        Program freelibrary = ProgramFactory.createProgram("aisco.program.activity.ProgramImpl", 2, "Free Library", "Library for underprivileged children ", "children", "BeriBuku Community", "https://www.myschool.splelive.id/liblogo");
        Program paymentelectricity = ProgramFactory.createProgram("aisco.program.operational.ProgramImpl", 3, "Electricity Payment", "Training for new teachers", "10 Teachers");


        //Create instance of Incomes
        List<FinancialReport> incomes = new ArrayList<>();
        incomes.add(FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl", "1", "23-10-2019", 100000, "Donation Ana", schoolconstruction, "11000", "Transfer"));
        incomes.add(FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl", "2", "24-10-2019", 3000000, "Donation Joni", freelibrary, "11000", "Cash"));
        incomes.add(FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl", "3", "11-11-2019", 5000000, "CSR Firma SJT", schoolconstruction, "110", "Transfer"));
        incomes.add(FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl", "3", "11-11-2019", 50000, "Donation Aida", paymentelectricity, "110", "Cash"));
        System.out.println("\n Income Report");
        System.out.println(Arrays.asList(incomes));
        income.printTotal();

        //Create instance of Expenses
        List<FinancialReport> expenses = new ArrayList<>();
        expenses.add(FinancialReportFactory.createFinancialReport("aisco.financialreport.expense.FinancialReportImpl", "10", "23-10-2019", 1000000, "Buy Cement", schoolconstruction, "41000"));
        expenses.add(FinancialReportFactory.createFinancialReport("aisco.financialreport.expense.FinancialReportImpl", "20", "24-10-2019", 1500000, "Buy Bookcase", freelibrary, "410"));
        expenses.add(FinancialReportFactory.createFinancialReport("aisco.financialreport.expense.FinancialReportImpl", "30", "12-10-2019", 50000, "Book ABC", freelibrary, "41000"));
        System.out.println("\n Expense Report");
        System.out.println(Arrays.toString(expenses.toArray()));
        expense.printTotal();

        System.out.println("\n Donation Report");
        donate.addDonation();
        donate.getDonation();

    }
}
