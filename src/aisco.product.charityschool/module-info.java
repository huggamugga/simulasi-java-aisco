module aisco.product.charityschool {
    requires aisco.program;
    requires aisco.financialreport; 
    requires aisco.donation;
    requires aisco.feature.selection;  
}