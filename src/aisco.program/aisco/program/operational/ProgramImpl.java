package aisco.program.operational;

import aisco.program.activity.ProgramDecorator;

public class ProgramImpl extends ProgramDecorator {

    public ProgramImpl()
    {
     System.out.println("Program Operational");
    }

    /** delta removes attributes, modifies the constructor */
    public ProgramImpl(Integer idProgram, String name, String description, String target)
    {
        super(idProgram, name, description, target);
    }

    /**delta removes method */
    public void setExecutionDate(String date)
    {
        throw new UnsupportedOperationException();
    }


    public String toString(){
        return " Operational " + name + "";
    }
}
