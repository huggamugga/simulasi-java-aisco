package aisco.program.activity;

public class ProgramDecorator extends ProgramComponent {

    public ProgramDecorator()
    {

    }
    /*delta removes attributes, modifies the constructor */
    public ProgramDecorator (Integer idProgram, String name, String description, String target) {
        this.idProgram = idProgram;
        this.name = name;
        this.description = description;
        this.target = target;
    }
}
