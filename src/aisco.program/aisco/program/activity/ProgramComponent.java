package aisco.program.activity;

public abstract class ProgramComponent implements Program{

    protected Integer idProgram;
    protected String name;
    protected String description;
    protected String target;
    protected String partner;
    protected String logoUrl;
    protected String executionDate;

    public ProgramComponent()
    {
    }

    public ProgramComponent(Integer idProgram, String name, String description, String target, String partner, String logoUrl)
    {
        this.idProgram = idProgram;
        this.name = name;
        this.description = description;
        this.target = target;
        this.partner = partner;
        this.logoUrl = logoUrl;
    }

    public void setExecutionDate(String date)
    {
        System.out.println(date);
        this.executionDate = date;
    }

}
