package aisco.donation;


import aisco.donation.core.Donation;
import java.lang.reflect.Constructor;
import java.util.logging.Logger;

public class DonationFactory
{
    private static final Logger LOGGER = Logger.getLogger(DonationFactory.class.getName());

    private DonationFactory()
    {

    }

    public static Donation createDonation (String fullyQualifiedName)
    {
        Donation record = null;
        try {
            Class<?> clz = Class.forName(fullyQualifiedName);
            System.out.println(clz);
            Constructor<?> constructor = clz.getDeclaredConstructors()[0];
            record = (Donation) constructor.newInstance();
        } catch (Exception e)
        {
            System.out.println(e);
            System.out.println("Failed to create instance of Donation Init.");
            LOGGER.severe("Given FQN: " + fullyQualifiedName);
            System.exit(30);
        }
        return record;
    }

    public static Donation createDonation(String fullyQualifiedName, String name, String email, String phone, Integer amount, String paymentMethod) {
        Donation record = null;
        try {
            Class<?> clz = Class.forName(fullyQualifiedName);
            Constructor<?> constructor1 = clz.getConstructor(String.class, String.class, String.class, Integer.class, String.class);
            record = (Donation) constructor1.newInstance(name, email, phone, amount,paymentMethod);
        }
        catch (NoSuchMethodException ex)
        {
            System.out.println("Method not found");
            System.exit(20);
        }
        catch (Exception ex) {
            System.out.println(ex);
            LOGGER.severe("Failed to create instance of Donation.");
            LOGGER.severe("Given FQN: " + fullyQualifiedName);
            System.exit(30);
        }

        return record;
    }

}