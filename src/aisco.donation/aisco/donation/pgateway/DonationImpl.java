package aisco.donation.pgateway;

import aisco.donation.core.Donation;
import aisco.donation.core.DonationDecorator;

import payment.page.core.*;



public class DonationImpl extends DonationDecorator {
    PaymentPage payment;

    public DonationImpl() {
        System.out.println("Donation via Payment Gateway");
        payment = PaymentPageFactory.createPaymentPage("payment.page.core.PaymentPageImpl");
    }

    public DonationImpl (String name, String email, String phone, Integer amount, String paymentMethod)
    {
        super(name, email, phone, amount,paymentMethod);
    }

    public void getDonation(){ 
        payment.getTransaction();
    }

    public void addDonation(){
        payment.addTransaction();
    }

    public String toString() {
        return "- Donasi " + name + ": " + amount + " Payment Method: " + paymentMethod + "\n";
    }
}
