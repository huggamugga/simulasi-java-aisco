package aisco.donation.core;

//import payment.page.core.*;
import aisco.donation.core.DonationComponent;



public abstract class DonationDecorator extends DonationComponent {
   // PaymentPage payment;

    public DonationDecorator() {
     super();
    }

    public DonationDecorator (String name, String email, String phone, Integer amount, String paymentMethod)
    {
        super(name, email, phone, amount,paymentMethod);
    }

    public abstract void getDonation();

    public abstract void addDonation();
    


}
