package aisco.feature.selection;

import java.util.ArrayList;
import java.util.List;

public class FeatureSelection {
    List<Object> featuresList;

    public FeatureSelection()
    {
        featuresList = new ArrayList<>();
        System.out.println("Selected Features: ");
    }

    public void productValidation(String product) throws Exception {
        //Write the features' constraints here
        if (featuresList.contains("aisco.financialreport.income.FinancialReportImpl") && featuresList.contains("aisco.program.activity.ProgramImpl")) {
            System.out.println("** Product " + product + " is valid and successfully created **");
        } else {
            throw new Exception("Mandatory features are not selected");
        }
    }

    public void addFeatures(Object object) {
        String features = object.getClass().getName();
        featuresList.add(features);
    }

}
