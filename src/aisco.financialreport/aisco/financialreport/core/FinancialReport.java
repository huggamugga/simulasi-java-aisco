package aisco.financialreport.core;

public interface FinancialReport {
    void run();
    void printTotal();
}
