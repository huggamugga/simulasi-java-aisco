package aisco.financialreport.expense;

import aisco.program.activity.Program;
import aisco.financialreport.core.FinancialReportDecorator;

public class FinancialReportImpl extends FinancialReportDecorator {
    private static int totalExpense;

    public FinancialReportImpl() {
        super();
    }

    /*delta removes attributes, modifies the constructor */
    public FinancialReportImpl(String id, String dateStamp, Integer amount, String description, Program idProgram, String idCoa) {
        super(id, dateStamp, amount, description, idProgram, idCoa);
        addAmount(amount);
    }

    public void addAmount(int amount) {
        totalExpense += amount;
    }

    public void printTotal() {
        System.out.println("Total Expense: " + totalExpense);
    }

    @Override
    public void run() {
        System.out.println("Expense Report");
    }
}



