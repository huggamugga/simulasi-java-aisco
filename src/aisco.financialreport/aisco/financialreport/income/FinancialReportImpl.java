package aisco.financialreport.income;

import aisco.program.activity.Program;
import aisco.financialreport.core.FinancialReportDecorator;
import payment.page.core.*;

public class FinancialReportImpl extends FinancialReportDecorator {
    /* delta add attributes */
    private String paymentMethod;
    private static int totalIncome;

    public FinancialReportImpl() {
        super();
    }

    /*delta adds attributes, modifies the constructor */
    public FinancialReportImpl(String id, String dateStamp, Integer amount, String description, Program idProgram, String idCoa, String paymentMethod) {
        super(id, dateStamp, amount, description, idProgram, idCoa);
        this.paymentMethod = paymentMethod;
        addAmount(amount);
    }

    /* delta modifies method */
    @Override
    public void run() {
        System.out.println("Income Report");
    }

    /* Implementation of New Method in Decorator Class */
    public void addAmount(int amount) {
        totalIncome+=amount;
    }

    public void printTotal()
    {
        System.out.println("Total income: "+totalIncome);
    }

    public String toString() {
        return "- " + description + ": " + amount + " for Program" + idProgram + ". Payment Method: " + paymentMethod + "\n";
    }

    public static void main (String args[])
    {
        PaymentPage page2 = PaymentPageFactory.createPaymentPage("payment.page.core.PaymentPageImpl");
    }
}


