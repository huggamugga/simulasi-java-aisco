package aisco.financialreport;
import aisco.program.activity.Program;
import aisco.financialreport.coa.FinancialReportCoaDecorator;
import aisco.financialreport.core.FinancialReport;

import java.lang.reflect.Constructor;
import java.util.logging.Logger;



public class FinancialReportFactory {
    public FinancialReportCoaDecorator decorate;
    private static final Logger LOGGER = Logger.getLogger(FinancialReportFactory.class.getName());

    private FinancialReportFactory()
    {

    }

    /** initiate features **/
    public static FinancialReport createFinancialReport(String fullyQualifiedName)
    {   FinancialReport record = null;
        try {
            Class<?> clz = Class.forName(fullyQualifiedName);
            Constructor<?> constructor = clz.getDeclaredConstructors()[0];
            record = (FinancialReport) constructor.newInstance();
        } catch (Exception e)
        {
            System.out.println(e);
            System.out.println("Failed to create instance of Financial Report.");
            LOGGER.severe("Given FQN: " + fullyQualifiedName);
            System.exit(30);
        }
        return record;
    }


    public static FinancialReport createFinancialReport(String fullyQualifiedName, String id, String dateStamp, Integer amount, String description, Program idProgram, String idCoa) {
        FinancialReport record = null;
        try {
            Class<?> clz = Class.forName(fullyQualifiedName);
            Constructor<?> constructor1 = clz.getConstructor(String.class, String.class, Integer.class, String.class, Program.class, String.class);
            record = (FinancialReport) constructor1.newInstance(id, dateStamp, amount, description, idProgram, idCoa);
            FinancialReportCoaDecorator decorate= new FinancialReportCoaDecorator (record);
            decorate.checkCoa();
        }
        catch (NoSuchMethodException ex)
        {
            System.out.println("Method not found");
            System.exit(20);
        }
        catch (Exception ex) {
            //System.out.println("Hello Catch");
            System.out.println(ex);
            LOGGER.severe("Failed to create instance of FinancialReport.");
            LOGGER.severe("Given FQN: " + fullyQualifiedName);
            System.exit(30);
        }

        return record;
    }

    /**Modifiy Factory Method because there is a new attributes in Income **/
    public static FinancialReport createFinancialReport(String fullyQualifiedName, String id, String dateStamp, Integer amount, String description, Program idProgram, String idCoa, String paymentMethod) {
        FinancialReport record = null;
        try {
            Class<?> clz = Class.forName(fullyQualifiedName);
            //System.out.println(clz);
            Constructor<?> constructor2 = clz.getConstructor(String.class, String.class, Integer.class, String.class, Program.class, String.class, String.class);
            record = (FinancialReport) constructor2.newInstance(id, dateStamp, amount, description, idProgram, idCoa, paymentMethod);
            FinancialReport decorate= new FinancialReportCoaDecorator (record);
        }
        catch (NoSuchMethodException ex)
        {
            System.out.println("Method not found");
            System.exit(20);
        }
        catch (Exception ex) {
            //System.out.println("Hello Catch");
            System.out.println(ex);
            LOGGER.severe("Failed to create instance of FinancialReport. Returning base FinancialReport.");
            LOGGER.severe("Given FQN: " + fullyQualifiedName);
            System.exit(30);
        }

        return record;
    }
}
