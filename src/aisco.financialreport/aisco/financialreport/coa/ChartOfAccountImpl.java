package aisco.financialreport.coa;

import java.util.ArrayList;
import java.util.List;

public class ChartOfAccountImpl implements ChartOfAccount {

    String idChartOfAccount;
    String name;
    String description;

    public ChartOfAccountImpl()
    {

    }

    public ChartOfAccountImpl(String idChartOfAccount, String name, String description)
    {
        this.idChartOfAccount = idChartOfAccount;
        this.name = name;
        this.description = description;
    }

    public void addCoa()
    {
        System.out.println("Chart of Account created");
        List <ChartOfAccount> coa = new ArrayList<>();
        ChartOfAccount coa1 = new ChartOfAccountImpl("42010", "Donasi", "Sumbangan dari Donatur");
        ChartOfAccount coa2 = new ChartOfAccountImpl("10000", "Aset", "Aset yang dimiliki");
        ChartOfAccount coa3 = new ChartOfAccountImpl("42000", "Peggalangan Dana (Hadiah)", "Penggalangan Dana atau Hadiah dari Donatur Grup");
        ChartOfAccount coa4 = new ChartOfAccountImpl("50200", "Pembelian", "Pembelian barang habis pakai");
        ChartOfAccount coa5 = new ChartOfAccountImpl("60000", "Pengeluaran", "Pengeluaran organisasi");

        coa.add(coa1);
        coa.add(coa2);
        coa.add(coa3);
        coa.add(coa4);
        coa.add(coa5);
    }
}
