package aisco.financialreport.coa;
import aisco.financialreport.coa.ChartOfAccount;
import aisco.financialreport.core.FinancialReport;
import aisco.financialreport.core.FinancialReportDecorator;
import aisco.financialreport.coa.ChartOfAccountImpl;



public class FinancialReportCoaDecorator extends FinancialReportDecorator
{
    protected FinancialReport finance;
    protected ChartOfAccount idCoa;



    public FinancialReportCoaDecorator (FinancialReport finance)
    {
        this.finance=finance;
    }

    public void run(){

    }

    public void checkCoa()
    {

    }

    /** delta Remove Method **/
    public void printTotal()
    {
        throw new UnsupportedOperationException();
    }

    /** delta Remove Method **/
    public void addAmount(int amount)
    {
        throw new UnsupportedOperationException();
    }
}