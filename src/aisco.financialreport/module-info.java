module aisco.financialreport {
    exports aisco.financialreport.core;
    exports aisco.financialreport.coa;
    exports aisco.financialreport;
    requires java.logging;
    requires aisco.program;
    requires payment.page;
}



