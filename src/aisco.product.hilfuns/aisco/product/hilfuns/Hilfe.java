package aisco.product.hilfuns;

import aisco.feature.selection.FeatureSelection;
import aisco.program.activity.Program;
import aisco.program.ProgramFactory;
import aisco.financialreport.core.FinancialReport;
import aisco.financialreport.FinancialReportFactory;
import aisco.financialreport.coa.ChartOfAccount;
import aisco.financialreport.coa.ChartOfAccountImpl;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Hilfe {
    FeatureSelection features;
    List<Object> featuresList;

    public static void main(String[] args) {

        ChartOfAccount coa  = new ChartOfAccountImpl();
        coa.addCoa();
        //Select features by calling default constructor
        //Product Charity School consists of Feature (Activity, Income, Expense)
        FeatureSelection features = new FeatureSelection();
        Program program = ProgramFactory.createProgram("aisco.program.activity.ProgramImpl");
        FinancialReport income = FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl");
        FinancialReport expense = FinancialReportFactory.createFinancialReport("aisco.financialreport.expense.FinancialReportImpl");

        
        features.addFeatures(program);
        features.addFeatures(income);
        features.addFeatures(expense);    


        //Product Validation (Check Feature's constraints)
        try {
            features.productValidation("Charity School");
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(10);
        }

        //Create instance of program
        Program disasterrelief= ProgramFactory.createProgram("aisco.program.activity.ProgramImpl", 1, "Flood in Bogor", "Helping flood victims", "100 families", "Fast Furious", "https://www.hilfe.splelive.id/logohilfe");
        Program freemedicine = ProgramFactory.createProgram("aisco.program.activity.ProgramImpl", 2, "Free Medicine", "Free treatment and medicine for poor people", "50 persons", "Healthy Clinic", "https://www.hilfe.splelive.id/logopengobatan");

        //Create instance of Incomes
        List<FinancialReport> incomes = new ArrayList<>();
        incomes.add(FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl", "1", "23-11-2019", 1000000, "Donation Nadia", disasterrelief, "42010", "Transfer"));
        incomes.add(FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl", "2", "25-11-2019", 800000, "Donation Lentera", freemedicine, "42010", "Cash"));
        incomes.add(FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl", "2", "29-11-2019", 5000000, "Fundraising Mitra University", disasterrelief, "42000", "Transfer"));
        System.out.println("\n Income Report");
        System.out.println(Arrays.asList(incomes));
        income.printTotal();

        //Create instance of Expenses
        List<FinancialReport> expenses = new ArrayList<>();
        expenses.add(FinancialReportFactory.createFinancialReport("aisco.financialreport.expense.FinancialReportImpl", "10", "29-11-2019", 1000000, "Rice", disasterrelief, "50200"));
        expenses.add(FinancialReportFactory.createFinancialReport("aisco.financialreport.expense.FinancialReportImpl", "20", "30-11-2019", 350000, "Paracetamol", freemedicine, "410"));
        expenses.add(FinancialReportFactory.createFinancialReport("aisco.financialreport.expense.FinancialReportImpl", "30", "30-11-2019", 3500000, "Mattress and Blanket", disasterrelief, "50200"));
        System.out.println("\n Expense Report");
        System.out.println(Arrays.toString(expenses.toArray()));
        expense.printTotal();

    }
}
