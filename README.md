# SPL Implementation with Java Standard Development

## Introduction

We attempt to implement delta-oriented software product line (SPL) with Java programming language.
The idea is start from solving challenge case about interoperability of SPL. The init repository is [here](https://gitlab.com/mayaretno/splc-challenge-mspl/tree/java9). The case study is about a railway system that include Signal, Switch, and Station. 

In this repository, we use case study Adaptive Information System for Charity Organization (AISCO).

The features in AISCO are:
1. Program: Regular Activity (_mandatory_)
2. Program: Operational
3. Financial Report: Income (_mandatory_)
4. Financial Report: Expense
5. Donation: Core
6. Donation: Payment Gateway (call another product line)

There are three products:
1. Charity School that contains features Activity, Operational, Income, Expense, Payment Gateway.
2. Yayasan Pandhu that contains features Activity, Income.
3. Hilfuns that contains features Activity, Income, Expense.


## Modeling SPL with Java
- Product line is created as a Java Project. 
- Root Features in feature diagram are created as a Java module. Each child feature is created as Java package inside the module. 
- Delta modules are created inside the feature package, using Decorator Pattern. 
- Product is defined as a Java module. Each product variation is created as Java package inside the Product module.
- Feature Selection is created as Java module to handle the product validation. A simple feature's constraint is defined in a Java Class and it is called during product derivation.

### Product Derivation
Product derivation is started by calling the main method inside the specific product variation class. Feature selection process is done by adding default constructor for each feature. If product validation is success, the product is run succesfully. 
Choose your product, build the artifact, and run the product. 


### Structure Directory. 
Modules inside `src` folder:
- aisco.financialreport
    - income
    - expense
    - core
    - coa
- aisco.program
    - activity
    - operational
- aisco.donation
    -  core
    -  paymentgateway
- aisco.feature.selection 
    - Feature selection (feature model and constraint checker)
- aisco.product.charityschool
    - Product: Charity School
- aisco.product.yayasanpandhu
    - Product: Yayasan Pandhu
- aisco.product.hilfuns
    - Product: Hilfuns
Note: external modules are available inside folder `external`.

Notes: external modules are available inside folder `external`.

### How to Build Jar and Run (Product Generation)
Build: `bash genproduct.sh [ProductNameModule] [ProductName]`

Run: `java --module-path [ProductNameModule] -m [ProductNameModule]`


List of ProductNameModule and Product Name (Main Class)
```
Module aisco.product.charityschool | CharitySchool
Module aisco.product.yayasanpandhu | YayasanPandhu
Module aisco.product.hilfuns | Hilfe
```

#### Example:
Build: `bash genproduct.sh aisco.product.charityschool CharitySchool`

Run: `java --module-path aisco.product.charityschool -m aisco.product.charityschool`
    
   
### How to Compile and Run
Requirements: Java 11 or Later. 

Compile all modules: `javac -d mods --module-source-path src $(find src -name "*.java")`

Run Charity School: `java --module-path mods -m aisco.product.charityschool/aisco.product.charityschool.CharitySchool`

Run Yayasan Pandhu: `java --module-path mods -m aisco.product.yayasanpandhu/aisco.product.yayasanpandhu.YayasanPandhu`

The examples can also be run inside a Docker container:

```bash
docker build --tag aisco/java-example:latest .
docker run --rm -it aisco/java-example:latest

# Now inside the interactive terminal

## Compile all modules:
javac -d mods --module-source-path src $(find src -name "*.java")

## To run 'Charity School' product example:
java --module-path mods -m aisco.product.charityschool/aisco.product.charityschool.CharitySchool

## To run 'Yayasan Pandhu' product example:
java --module-path mods -m aisco.product.yayasanpandhu/aisco.product.yayasanpandhu.YayasanPandhu
```