function external_module()
{
    if [ $1 == "aisco.financialreport" ] || [ $1 == "aisco.donation" ]; then
    cp external/page.jar $product/
    echo "Add external module"
    fi
}
function build_product_requirement(){
  product=$1
  targetpath="src/$product/module-info.java"
  req=$(cat $targetpath | grep requires| awk '{print $2}' | cut -d';' -f 1 )
  for reqprod in $req; do
    echo -e "building requirement for $product: $reqprod"
    external_module $reqprod
    javac -d classes --module-path $product $(find src/$reqprod -name "*.java") src/$reqprod/module-info.java 
    jar --create --file $product/$reqprod.jar -C classes .
    rm -r classes
  done
  echo "requirement building done"
}


product=$1
mainclass=$2
if [ -d "$1" ]; then rm -r $1; fi
if [ -d "classes" ]; then rm -r classes; fi 
mkdir $1
echo "building req"
build_product_requirement $product
echo -e "building the product: $product"
javac -d classes  --module-path $1 $(find src/$product -name "*.java") src/$product/module-info.java 
jar --create --file $1/$2.jar --main-class $product.$mainclass -C classes .
rm -r classes
echo "Product $2 is ready"

